# frozen_string_literal: true

require_relative "lib/all_hours/version"

Gem::Specification.new do |spec|
  spec.name = 'all_hours'
  spec.version = AllHours::VERSION
  spec.summary = 'Everhour API access'
  spec.description = <<~DESC
    Gain access to the Everhour API from your Ruby application.
  DESC
  spec.author = "T S Vallender"
  spec.license = "MIT"
  spec.email = "trevor@tsvallender.co.uk"
  # spec.homepage = ""
  # spec.metadata = {
  #   "bug_tracker_uri"   => "",
  #   "changelog_uri"     => "",
  #   "documentation_uri" => "",
  #   "homepage_uri"      => spec.homepage,
  #   "source_code_uri"   => ""
  # }
  spec.files = Dir["lib/**/*"]

  spec.add_development_dependency 'rake'
  spec.add_development_dependency 'rspec'
  spec.add_development_dependency 'dotenv'
  spec.add_development_dependency 'webmock'
  spec.add_development_dependency 'vcr'
end