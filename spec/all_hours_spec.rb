require 'spec_helper'

describe AllHours do
  let(:api) { AllHours::Everhour.new api_key: ENV.fetch('API_KEY') }

  it "should get project" do
    VCR.use_cassette "get_project" do
      project = AllHours::Project.find api: api, project_id: ENV.fetch('PROJECT_ID')
      expect(project).to respond_to(:name)
      expect(project).to respond_to(:budget)
    end
  end

  it "should get all projects" do
    VCR.use_cassette "get_all_projects" do
      projects = AllHours::Project.all api: api
      expect(projects.length).to be > 3
      projects.each do |project|
        expect(project).to respond_to(:name)
        expect(project).to respond_to(:budget)
      end
    end
  end

  it "should get all clients" do
    VCR.use_cassette "get_all_clients" do 
      clients = AllHours::Client.all api: api
      clients.each do |client|
        expect(client).to respond_to(:name)
        expect(client).to respond_to(:project_ids)
      end
    end
  end

  it "should get the task" do
    VCR.use_cassette "get_task" do
      task = AllHours::Task.find api: api, task_id: ENV.fetch('TASK_ID')
      expect(task).to respond_to(:labels)
      expect(task).to respond_to(:project_ids)
    end
  end

  it "should get all project’s tasks" do
    VCR.use_cassette "get_all_projects_tasks" do
      project = AllHours::Project.find(api: api, project_id: ENV.fetch('PROJECT_ID'))
      tasks = project.tasks
      tasks.each do |task|
        expect(task).to respond_to(:labels)
        expect(task).to respond_to(:project_ids)
      end
    end
  end

  it "should search the tasks" do
    VCR.use_cassette "search_tasks" do
      tasks = AllHours::Task.search(api: api, query: 'Support')
      tasks.each do |task|
        expect(task).to respond_to(:labels)
        expect(task).to respond_to(:project_ids)
      end
    end
  end
end