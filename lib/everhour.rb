require 'net/http'
require 'all_hours/client'
require 'all_hours/project'
require 'all_hours/task'

module AllHours
  class Everhour
    private
    attr_accessor :api_key, :api_url, :api_version

    public

    def initialize api_key:, api_url: 'https://api.everhour.com/', api_version: '1.2'
      @api_key = api_key
      @api_url = api_url
      @api_version = api_version
    end

    def api_call path:, method: 'get', query: nil
      url = build_url path: path, query: query

      case method
      when 'get'
        req = Net::HTTP::Get.new(url.to_s)
        req['X-Api-Key'] = api_key
        req['X-Accept-Version'] = api_version
        req['Content-Type'] = 'application/json'
      else
        "Error: Unknown method “#{method}”."
      end

      res = Net::HTTP.start(url.host, url.port, use_ssl: true) { |http|
        http.request req
      }

      JSON.parse res.body
    end

    private

    def build_url path:, query: nil
      url = api_url + path

      unless query.nil?
        url = url + '?'
        i = 0
        query.each do |k, v|
          unless v.nil?
            url += '&' if i > 0
            url += "#{k}=#{v}"
          end
          i += 1
        end
      end

      URI.parse url
    end
  end
end