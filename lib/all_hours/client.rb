require 'net/http'
require 'json'
require 'date'

module AllHours
  class Client
    attr_accessor :project_ids,
                  :id,
                  :account,
                  :externalId,
                  :name,
                  :createdAt,
                  :lineItemMask,
                  :paymentDueDays,
                  :businessDetails,
                  :invoicePublicNotes,
                  :excludedLabels,
                  :status,
                  :enableResourcePlanner,
                  :favorite

    def initialize data:, api:
      @project_ids = data["project_ids"]
      @id = data["id"]
      @account = data["account"]
      @externalId = data["externalId"]
      @name = data["name"]
      @createdAt = Date.parse data["createdAt"]
      @lineItemMask = data["lineItemMask"]
      @paymentDueDays = data["paymentDueDays"]
      @businessDetails = data["businessDetails"]
      @invoicePublicNotes = data["invoicePublicNotes"]
      @excludedLabels = data["excludedLabels"]
      @status = data["status"]
      @enableResourcePlanner = data["enableResourcePlanner"]
      @favorite = data["favorite"]
    end

    def self.all api:, query: nil
      data = api.api_call path: "projects", query: {query: query}
      clients = []
      data.each do |client_data|
        clients << Client.new(api: api, data: client_data)
      end

      clients
    end
  end
end