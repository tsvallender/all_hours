require 'net/http'
require 'json'
require 'date'

module AllHours
  class Project
    attr_accessor :api,
                  :canSyncTasks,
                  :user_ids,
                  :id,
                  :platform,
                  :name,
                  :createdAt,
                  :workspaceId,
                  :workspaceName,
                  :hasWebhook,
                  :status,
                  :estimatesType,
                  :budget,
                  :enableResourcePlanner,
                  :isTemplate,
                  :privacy,
                  :client,
                  :favorite,
                  :foreign

    def initialize data:, api:
      @api = api
      @canSyncTasks = data["canSyncTasks"]
      @user_ids = data["users"]
      @id = data["id"]
      @platform = data["platform"]
      @name = data["name"]
      @createdAt = Date.parse data["createdAt"]
      @workspaceId = data["workspaceId"]
      @workspaceName = data["workspaceName"]
      @hasWebhook = data["hasWebhook"]
      @status = data["status"]
      @estimatesType = data["estimatesType"]
      @budget = data["budget"]
      @enableResourcePlanner = data["enableResourcePlanner"]
      @isTemplate = data["isTemplate"]
      @privacy = data["privacy"]
      @client = data["client"]
      @favorite = data["favorite"]
      @foreign = data["foreign"]
    end

    def self.find api:, project_id:
      data = api.api_call path: "projects/#{project_id}"
      Project.new api: api, data: data
    end

    def self.all api:, limit: 200, query: nil, platform: nil
      data = api.api_call path: "projects", query: {limit: limit, query: query, platform: platform}
      projects = []
      data.each do |project_data|
        projects << Project.new(api: api, data: project_data)
      end

      projects
    end

    def tasks limit: 250, page: 1, excludeClosed: true, query: nil
      data = @api.api_call(path: "projects/#{@id}/tasks",
                           query: {limit: limit, page: page,
                                   excludeClosed: excludeClosed, query: query})
      tasks = []
      data.each do |task_data|
        tasks << Task.new(api: api, data: task_data)
      end

      tasks
    end

    def url
      "https://app.everhour.com/#/projects/#{id}"
    end

    def budgeted_minutes
      budget.nil? ? 0 : budget["budget"] / 60
    end

    def remaining_minutes
      budget.nil? ? 0 : budgeted_minutes - (budget["progress"] / 60)
    end

    def percentage_used
      return 0 unless budgeted_minutes&.positive? && remaining_minutes
      (((budgeted_minutes.to_f - remaining_minutes) / budgeted_minutes) * 100).round
    end
  end
end