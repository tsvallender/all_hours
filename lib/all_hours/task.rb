require 'net/http'
require 'json'
require 'date'

module AllHours
  class Task
    attr_accessor :id,
                  :name,
                  :project_ids,
                  :section,
                  :labels,
                  :position,
                  :description,
                  :dueAt,
                  :status,
                  :tags,
                  :time,
                  :estimate,
                  :attributes,
                  :metrics,
                  :unbillable

    def initialize data:, api:
      @api = api
      @id = data["id"]
      @name = data["name"]
      @project_ids = data["project_ids"]
      @section = data["section"]
      @labels = data["labels"]
      @tags = data["tags"]
      @position = data["position"]
      @description = data["description"]
      @dueAt = Date.parse data["dueAt"] unless data["dueAt"].nil?
      @status = data["status"]
      @time = data["time"]
      @estimate = data["estimate"]
      @attributes = data["attributes"]
      @metrics = data["metrics"]
      @unbillable = data["unbillable"]
    end

    def self.find api:, task_id:
      data = api.api_call path: "tasks/#{task_id}"
      Task.new api: api, data: data
    end

    def self.search api:, limit: 100, query: nil, searchInClosed: false
      data = api.api_call(path: "tasks/search", query: {limit: limit, query: query, searchInClosed: searchInClosed})
      tasks = []
      data.each do |task_data|
        tasks << Task.new(api: api, data: task_data)
      end

      tasks
    end
  end
end